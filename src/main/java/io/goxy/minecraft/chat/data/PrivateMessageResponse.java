package io.goxy.minecraft.chat.data;

import java.util.UUID;

public class PrivateMessageResponse {
    private int messageId;
    private MessagePlayer sender;
    private MessagePlayer receiver;
    private String message;

    public PrivateMessageResponse() {
    }

    public PrivateMessageResponse(int messageId, MessagePlayer sender, MessagePlayer receiver, String message) {
        this.messageId = messageId;
        this.sender = sender;
        this.receiver = receiver;
        this.message = message;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public MessagePlayer getSender() {
        return sender;
    }

    public void setSender(MessagePlayer sender) {
        this.sender = sender;
    }

    public MessagePlayer getReceiver() {
        return receiver;
    }

    public void setReceiver(MessagePlayer receiver) {
        this.receiver = receiver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
