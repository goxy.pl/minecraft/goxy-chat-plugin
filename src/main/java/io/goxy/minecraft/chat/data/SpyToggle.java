package io.goxy.minecraft.chat.data;

import java.util.UUID;

public class SpyToggle {
    private UUID playerId;
    private boolean enable;

    public SpyToggle() {
    }

    public SpyToggle(UUID playerId, boolean enable) {
        this.playerId = playerId;
        this.enable = enable;
    }

    public UUID getPlayerId() {
        return playerId;
    }

    public void setPlayerId(UUID playerId) {
        this.playerId = playerId;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}
