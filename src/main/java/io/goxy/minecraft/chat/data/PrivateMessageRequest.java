package io.goxy.minecraft.chat.data;

import java.time.ZonedDateTime;

public class PrivateMessageRequest {
    private int messageId;
    private MessagePlayer sender;
    private MessagePlayer receiver;
    private String message;
    private ZonedDateTime sentAt;

    public PrivateMessageRequest() {
    }

    public PrivateMessageRequest(int messageId, MessagePlayer name, MessagePlayer receiver, String message, ZonedDateTime sentAt) {
        this.messageId = messageId;
        this.sender = name;
        this.receiver = receiver;
        this.message = message;
        this.sentAt = sentAt;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public MessagePlayer getSender() {
        return sender;
    }

    public void setSender(MessagePlayer sender) {
        this.sender = sender;
    }

    public MessagePlayer getReceiver() {
        return receiver;
    }

    public void setReceiver(MessagePlayer receiver) {
        this.receiver = receiver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ZonedDateTime getSentAt() {
        return sentAt;
    }

    public void setSentAt(ZonedDateTime sentAt) {
        this.sentAt = sentAt;
    }
}
