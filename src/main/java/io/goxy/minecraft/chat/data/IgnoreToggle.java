package io.goxy.minecraft.chat.data;

import java.util.UUID;

public class IgnoreToggle {
    private MessagePlayer player;
    private String ignoreName;
    boolean ignore;

    public IgnoreToggle() {
    }

    public IgnoreToggle(MessagePlayer player, String ignoreName, boolean ignore) {
        this.player = player;
        this.ignoreName = ignoreName;
        this.ignore = ignore;
    }

    public MessagePlayer getPlayer() {
        return player;
    }

    public String getIgnoreName() {
        return ignoreName;
    }

    public void setIgnoreName(String ignoreName) {
        this.ignoreName = ignoreName;
    }

    public boolean isIgnore() {
        return ignore;
    }

    public void setIgnore(boolean ignore) {
        this.ignore = ignore;
    }
}
