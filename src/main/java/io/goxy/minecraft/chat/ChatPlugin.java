package io.goxy.minecraft.chat;

import io.goxy.minecraft.chat.data.IgnoreToggle;
import io.goxy.minecraft.chat.data.PrivateMessageRequest;
import io.goxy.minecraft.chat.data.PrivateMessageResponse;
import io.goxy.minecraft.chat.data.SpyToggle;
import io.goxy.minecraft.chat.module.Ignore;
import io.goxy.minecraft.chat.module.PrivateMessage;
import io.goxy.minecraft.chat.module.Reply;
import io.goxy.minecraft.chat.module.Spy;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import pl.goxy.minecraft.pubsub.PubSub;
import pl.goxy.minecraft.pubsub.PubSubService;

import java.util.Objects;

public class ChatPlugin extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        this.saveDefaultConfig();

        PluginManager pluginManager = this.getServer().getPluginManager();

        PubSub pubSub;
        if (pluginManager.isPluginEnabled("goxy-pubsub")) {
            PubSubService pubSubService = (PubSubService) Objects.requireNonNull(
                    pluginManager.getPlugin("goxy-pubsub"));
            pubSub = pubSubService.getPubSub(this);
        } else {
            pubSub = null;
        }

        ChatConfiguration configuration = getConfiguration();

        Ignore ignore = new Ignore(configuration, pubSub);
        PrivateMessage privateMessage = new PrivateMessage(configuration, this, pubSub, ignore);
        Reply reply = new Reply(configuration, privateMessage);
        Spy spy = new Spy(configuration, this, pubSub);

        this.getCommand("private-message").setExecutor(privateMessage);
        this.getCommand("reply").setExecutor(reply);
        this.getCommand("spy").setExecutor(spy);
        this.getCommand("ignore").setExecutor(ignore);

        if (pubSub != null) {
            pubSub.registerHandler(PrivateMessage.PUB_SUB_REQUEST_HANDLER, PrivateMessageRequest.class, (context, data) ->
            {
                privateMessage.handleRequest(data);
            }).async(false);
            pubSub.registerHandler(PrivateMessage.PUB_SUB_RESPONSE_HANDLER, PrivateMessageResponse.class, (context, data) ->
            {
                String serverName = context.getServer() != null ? context.getServer().getName() : null;
                spy.handleMessageResponse(data, serverName);

                privateMessage.handleResponse(data);
                reply.handleMessageResponse(data);
            }).async(false);
            pubSub.registerHandler(Spy.PUB_SUB_HANDLER, SpyToggle.class, (context, data) ->
            {
                spy.handleToggle(data);
            }).async(false);
            pubSub.registerHandler(Ignore.PUB_SUB_HANDLER, IgnoreToggle.class, (context, data) ->
            {
                ignore.handleToggle(data);
            }).async(false);
        }
    }

    @NotNull
    private ChatConfiguration getConfiguration() {
        FileConfiguration config = this.getConfig();

        ChatConfiguration configuration = new ChatConfiguration();
        configuration.setMessageCommandUsage(config.getString("command.message.usage"));
        configuration.setMessageIgnored(config.getString("command.message.ignored"));
        configuration.setMessagePlayerOffline(config.getString("command.message.player-offline"));

        configuration.setSpyEnabled(config.getString("command.spy.enabled"));
        configuration.setSpyDisabled(config.getString("command.spy.disabled"));

        configuration.setReplyCommandUsage(config.getString("command.reply.usage"));
        configuration.setNoOneToReply(config.getString("command.reply.no-one-to-reply"));

        configuration.setIgnoreCommandUsage(config.getString("command.ignore.usage"));
        configuration.setIgnoreEnabled(config.getString("command.ignore.enabled"));
        configuration.setIgnoreDisabled(config.getString("command.ignore.disabled"));

        configuration.setMessageFormat(config.getString("format.message"));
        configuration.setSpyFormat(config.getString("format.spy-message"));
        configuration.setSpyServerFormat(config.getString("format.spy-server-message"));
        return configuration;
    }
}
