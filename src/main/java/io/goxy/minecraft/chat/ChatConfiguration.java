package io.goxy.minecraft.chat;

public class ChatConfiguration {
    private String messageCommandUsage;
    private String messageIgnored;
    private String messagePlayerOffline;

    private String spyEnabled;
    private String spyDisabled;

    private String replyCommandUsage;
    private String noOneToReply;

    private String ignoreCommandUsage;
    private String ignoreEnabled;
    private String ignoreDisabled;

    private String messageFormat;
    private String spyFormat;
    private String spyServerFormat;

    public String getMessageCommandUsage() {
        return messageCommandUsage;
    }

    public void setMessageCommandUsage(String messageCommandUsage) {
        this.messageCommandUsage = messageCommandUsage;
    }

    public String getMessageIgnored() {
        return messageIgnored;
    }

    public void setMessageIgnored(String messageIgnored) {
        this.messageIgnored = messageIgnored;
    }

    public String getMessagePlayerOffline() {
        return messagePlayerOffline;
    }

    public void setMessagePlayerOffline(String messagePlayerOffline) {
        this.messagePlayerOffline = messagePlayerOffline;
    }

    public String getSpyEnabled() {
        return spyEnabled;
    }

    public void setSpyEnabled(String spyEnabled) {
        this.spyEnabled = spyEnabled;
    }

    public String getSpyDisabled() {
        return spyDisabled;
    }

    public void setSpyDisabled(String spyDisabled) {
        this.spyDisabled = spyDisabled;
    }

    public String getReplyCommandUsage() {
        return replyCommandUsage;
    }

    public void setReplyCommandUsage(String replyCommandUsage) {
        this.replyCommandUsage = replyCommandUsage;
    }

    public String getNoOneToReply() {
        return noOneToReply;
    }

    public void setNoOneToReply(String noOneToReply) {
        this.noOneToReply = noOneToReply;
    }

    public String getIgnoreCommandUsage() {
        return ignoreCommandUsage;
    }

    public void setIgnoreCommandUsage(String ignoreCommandUsage) {
        this.ignoreCommandUsage = ignoreCommandUsage;
    }

    public String getIgnoreEnabled() {
        return ignoreEnabled;
    }

    public void setIgnoreEnabled(String ignoreEnabled) {
        this.ignoreEnabled = ignoreEnabled;
    }

    public String getIgnoreDisabled() {
        return ignoreDisabled;
    }

    public void setIgnoreDisabled(String ignoreDisabled) {
        this.ignoreDisabled = ignoreDisabled;
    }

    public String getMessageFormat() {
        return messageFormat;
    }

    public void setMessageFormat(String messageFormat) {
        this.messageFormat = messageFormat;
    }

    public String getSpyFormat() {
        return spyFormat;
    }

    public void setSpyFormat(String spyFormat) {
        this.spyFormat = spyFormat;
    }

    public String getSpyServerFormat() {
        return spyServerFormat;
    }

    public void setSpyServerFormat(String spyServerFormat) {
        this.spyServerFormat = spyServerFormat;
    }
}
