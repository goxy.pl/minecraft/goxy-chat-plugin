package io.goxy.minecraft.chat.module;

import io.goxy.minecraft.chat.ChatConfiguration;
import io.goxy.minecraft.chat.ChatUtil;
import io.goxy.minecraft.chat.data.IgnoreToggle;
import io.goxy.minecraft.chat.data.MessagePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import pl.goxy.minecraft.pubsub.PubSub;

import java.util.*;

public class Ignore implements CommandExecutor {
    public static final String PUB_SUB_HANDLER = "chat-ignore";

    private final Map<String, Set<UUID>> ignoredByMap = new HashMap<>();

    private final ChatConfiguration configuration;
    private final PubSub pubSub;

    public Ignore(ChatConfiguration configuration, PubSub pubSub) {
        this.configuration = configuration;
        this.pubSub = pubSub;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("can not use this command from console");
            return true;
        }
        if (args.length < 1) {
            sender.sendMessage(ChatUtil.colorize(this.configuration.getIgnoreCommandUsage())
                    .replace("{label}", label));
            return true;
        }
        UUID playerId = ((Player) sender).getUniqueId();
        String ignoreName = args[0].toLowerCase(Locale.ROOT);

        boolean ignore = !this.ignoredByMap.getOrDefault(ignoreName, new HashSet<>()).contains(playerId);
        String ignoreMessage = ChatUtil.colorize(ignore ? this.configuration.getIgnoreEnabled() : this.configuration.getIgnoreDisabled())
                .replace("{player_name}", ignoreName);
        sender.sendMessage(ignoreMessage);

        IgnoreToggle toggle = new IgnoreToggle(new MessagePlayer(playerId, sender.getName()), ignoreName, ignore);
        if (this.pubSub != null) {
            this.pubSub.sendNetwork(PUB_SUB_HANDLER, toggle);
        } else {
            this.handleToggle(toggle);
        }
        return true;
    }

    public void handleToggle(IgnoreToggle toggle) {
        Set<UUID> ignoreData = this.ignoredByMap.computeIfAbsent(toggle.getIgnoreName(), ($) -> new HashSet<>());
        if (toggle.isIgnore()) {
            ignoreData.add(toggle.getPlayer().getId());
        } else {
            ignoreData.remove(toggle.getPlayer().getId());
        }
    }

    public boolean isPlayerOnIgnoreList(UUID sender, MessagePlayer receiver) {
        if (receiver.getName() != null) {
            String receiverName = receiver.getName().toLowerCase(Locale.ROOT);
            return this.ignoredByMap.getOrDefault(receiverName, new HashSet<>()).contains(sender);
        }
        return false;
    }
}
