package io.goxy.minecraft.chat.module;

import io.goxy.minecraft.chat.ChatConfiguration;
import io.goxy.minecraft.chat.ChatUtil;
import io.goxy.minecraft.chat.data.MessagePlayer;
import io.goxy.minecraft.chat.data.PrivateMessageResponse;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class Reply implements CommandExecutor {
    private final Map<UUID, MessagePlayer> replyHistory = new HashMap<>();

    private final ChatConfiguration configuration;
    private final PrivateMessage privateMessage;

    public Reply(ChatConfiguration configuration, PrivateMessage privateMessage) {
        this.configuration = configuration;
        this.privateMessage = privateMessage;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("can not use this command from console");
            return true;
        }
        if (args.length < 1) {
            sender.sendMessage(ChatUtil.colorize(this.configuration.getReplyCommandUsage())
                    .replace("{label}", label));
            return true;
        }
        UUID playerId = ((Player) sender).getUniqueId();
        MessagePlayer receiver = this.replyHistory.get(playerId);
        if (receiver == null) {
            sender.sendMessage(ChatUtil.colorize(this.configuration.getNoOneToReply()));
            return true;
        }

        String message = String.join(" ", args);
        this.privateMessage.sendMessage(sender, receiver, message);
        return true;
    }

    public void handleMessageResponse(PrivateMessageResponse data) {
        this.replyHistory.put(data.getReceiver().getId(), data.getSender());
    }
}
