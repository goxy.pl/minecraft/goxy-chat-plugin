package io.goxy.minecraft.chat.module;

import io.goxy.minecraft.chat.ChatConfiguration;
import io.goxy.minecraft.chat.ChatUtil;
import io.goxy.minecraft.chat.data.MessagePlayer;
import io.goxy.minecraft.chat.data.PrivateMessageResponse;
import io.goxy.minecraft.chat.data.SpyToggle;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;
import pl.goxy.minecraft.pubsub.PubSub;

import java.util.*;

public class Spy implements CommandExecutor {
    public static final String SPY_PERMISSION = "goxy.private-chat.spy";
    public static final String PUB_SUB_HANDLER = "chat-spy";

    private final Set<UUID> spyEnabled = new HashSet<>();

    private final ChatConfiguration configuration;
    private final Plugin plugin;
    private final PubSub pubSub;

    public Spy(ChatConfiguration configuration, Plugin plugin, PubSub pubSub) {
        this.configuration = configuration;
        this.plugin = plugin;
        this.pubSub = pubSub;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("can not use this command from console");
            return true;
        }
        UUID playerId = ((Player) sender).getUniqueId();

        boolean enable = !this.spyEnabled.contains(playerId);
        if (enable) {
            sender.sendMessage(ChatUtil.colorize(this.configuration.getSpyEnabled()));
        } else {
            sender.sendMessage(ChatUtil.colorize(this.configuration.getSpyDisabled()));
        }

        SpyToggle toggle = new SpyToggle(playerId, enable);
        if (this.pubSub != null) {
            this.pubSub.sendNetwork(PUB_SUB_HANDLER, toggle);
        } else {
            this.handleToggle(toggle);
        }
        return true;
    }
    public void handleToggle(SpyToggle toggle) {
        if (toggle.isEnable()) {
            this.spyEnabled.add(toggle.getPlayerId());
        } else {
            this.spyEnabled.remove(toggle.getPlayerId());
        }
    }

    public void handleMessageResponse(PrivateMessageResponse data, String serverName) {
        MessagePlayer sender = data.getSender();
        MessagePlayer receiver = data.getReceiver();

        String message = data.getMessage();

        String format = serverName == null ? this.configuration.getSpyFormat() : this.configuration.getSpyServerFormat();
        String messageContent = ChatUtil.colorize(format)
                .replace("{sender_name}", sender.getName())
                .replace("{message}", message)
                .replace("{receiver_name}", receiver.getName())
                .replace("{server_name}", serverName != null ? serverName : "null");

        for (UUID playerId : this.spyEnabled) {
            Player player = this.plugin.getServer().getPlayer(playerId);
            if (player != null && player.hasPermission(SPY_PERMISSION)) {
                player.sendMessage(messageContent);
            }
        }
    }
}
