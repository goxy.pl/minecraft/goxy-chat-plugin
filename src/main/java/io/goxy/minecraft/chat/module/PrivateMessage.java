package io.goxy.minecraft.chat.module;

import io.goxy.minecraft.chat.ChatConfiguration;
import io.goxy.minecraft.chat.ChatUtil;
import io.goxy.minecraft.chat.data.MessagePlayer;
import io.goxy.minecraft.chat.data.PrivateMessageRequest;
import io.goxy.minecraft.chat.data.PrivateMessageResponse;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;
import pl.goxy.minecraft.pubsub.PubSub;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class PrivateMessage implements CommandExecutor {
    public static final String SEND_COLOR_PERMISSION = "goxy.chat.message.color";
    public static final String PUB_SUB_REQUEST_HANDLER = "chat-message-request";
    public static final String PUB_SUB_RESPONSE_HANDLER = "chat-message-response";

    private static final long START_WAIT_FOR_RESPONSE = 1000;

    private final UUID consoleSender = UUID.randomUUID();
    private final Map<Integer, PrivateMessageRequest> messageRequests = new HashMap<>();
    private final AtomicInteger messageId = new AtomicInteger();

    private final ChatConfiguration configuration;
    private final Plugin plugin;
    private final PubSub pubSub;
    private final Ignore ignore;

    private long waitForResponse = START_WAIT_FOR_RESPONSE;

    public PrivateMessage(ChatConfiguration configuration, Plugin plugin, PubSub pubSub, Ignore ignore) {
        this.configuration = configuration;
        this.plugin = plugin;
        this.pubSub = pubSub;
        this.ignore = ignore;

        this.plugin.getServer().getScheduler().runTaskTimer(this.plugin, () -> {
            ZonedDateTime now = ZonedDateTime.now();

            Iterator<PrivateMessageRequest> iterator = this.messageRequests.values().iterator();
            while (iterator.hasNext()) {
                PrivateMessageRequest request = iterator.next();

                if (request.getSentAt().plus(this.waitForResponse, ChronoUnit.MILLIS).isBefore(now)) {
                    iterator.remove();
                    this.handleNoResponse(request);
                }
            }
        }, 2, 2);
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (args.length < 2) {
            sender.sendMessage(ChatUtil.colorize(this.configuration.getMessageCommandUsage())
                    .replace("{label}", label));
            return true;
        }

        String receiverName = args[0];
        String message = Arrays.stream(args).skip(1).collect(Collectors.joining(" "));

        this.sendMessage(sender, new MessagePlayer(null, receiverName), message);
        return true;
    }

    public void sendMessage(CommandSender sender, MessagePlayer receiver, String message) {
        UUID senderId = sender instanceof Player ? ((Player) sender).getUniqueId() : this.consoleSender;

        if (this.ignore.isPlayerOnIgnoreList(senderId, receiver)) {
            sender.sendMessage(ChatUtil.colorize(this.configuration.getMessageIgnored()));
            return;
        }
        if (sender.hasPermission(PrivateMessage.SEND_COLOR_PERMISSION)) {
            message = ChatUtil.colorize(message.trim());
        }

        Logger logger = this.plugin.getLogger();

        int messageId = this.messageId.getAndIncrement();
        MessagePlayer messageSender = new MessagePlayer(senderId, sender.getName());

        ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);
        PrivateMessageRequest messageRequest = new PrivateMessageRequest(messageId, messageSender, receiver, message, now);
        if (this.pubSub != null) {
            this.pubSub.sendPluginNetwork(PUB_SUB_REQUEST_HANDLER, messageRequest).whenComplete((x, throwable) ->
            {
                if (throwable != null) {
                    logger.log(Level.SEVERE, "Failed to send private chat message to pubsub", throwable);
                }
            });
        } else {
            this.handleRequest(messageRequest);
        }
        this.messageRequests.put(messageRequest.getMessageId(), messageRequest);
    }

    public void handleRequest(PrivateMessageRequest data) {
        int messageId = data.getMessageId();
        MessagePlayer messageSender = data.getSender();

        MessagePlayer messageReceiver = data.getReceiver();
        Player receiver;
        if (messageReceiver.getId() != null) {
            receiver = Bukkit.getPlayer(messageReceiver.getId());
        } else {
            receiver = Bukkit.getPlayer(messageReceiver.getName());
        }
        if (receiver == null) {
            return;
        }

        messageReceiver = new MessagePlayer(receiver.getUniqueId(), receiver.getName());

        String message = data.getMessage();
        PrivateMessageResponse messageResponse = new PrivateMessageResponse(messageId, messageSender, messageReceiver, message);
        if (this.pubSub != null) {
            this.pubSub.sendNetwork(PUB_SUB_RESPONSE_HANDLER, messageResponse);
        } else {
            this.handleResponse(messageResponse);
        }

        String format = this.configuration.getMessageFormat();
        String messageContent = ChatUtil.colorize(format)
                .replace("{sender_name}", messageSender.getName())
                .replace("{message}", message)
                .replace("{receiver_name}", receiver.getName());

        receiver.sendMessage(messageContent);
    }

    public void handleResponse(PrivateMessageResponse data) {
        this.messageRequests.remove(data.getMessageId());

        MessagePlayer messageSender = data.getSender();
        MessagePlayer messageReceiver = data.getReceiver();

        Player sender = Bukkit.getPlayer(messageSender.getId());
        if (sender == null) {
            return;
        }

        String receiverName = messageReceiver.getName();

        String message = data.getMessage();

        String format = this.configuration.getMessageFormat();
        String messageContent = ChatUtil.colorize(format)
                .replace("{sender_name}", sender.getName())
                .replace("{message}", message)
                .replace("{receiver_name}", receiverName);
        sender.sendMessage(messageContent);
    }

    public void handleNoResponse(PrivateMessageRequest request) {
        CommandSender sender;
        if (request.getSender().getId().equals(this.consoleSender)) {
            sender = Bukkit.getConsoleSender();
        } else {
            sender = Bukkit.getPlayer(request.getSender().getId());
        }
        if (sender == null) {
            return;
        }

        String receiverName = request.getReceiver().getName();

        String format = this.configuration.getMessagePlayerOffline();
        String messageContent = ChatUtil.colorize(format).replace("{player_name}", receiverName);

        sender.sendMessage(messageContent);
    }
}
